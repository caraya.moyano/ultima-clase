INSERT INTO "VMI-CPFR".permiso (id,nombre_permiso,activo) VALUES
	 ('d507f8e2-f15b-498e-9038-e07f51e1857d','CPFR',true),
	 ('2ace12eb-5fb2-45ba-b1b4-457b0193b574','VMI-Calculo',true),
	 ('04e7ff22-7b61-4e0f-a97b-c39699c4c21d','VMI-Parametros',true),
	 ('85efa2c8-8831-4b3b-806a-c9c5aad0b28e','VMI-GenerarPedido',true),
	 ('42aad499-5828-4276-bc93-74f826b6f5b4','VMI-AlmacenPedidos',true),
	 ('11b63a8c-7f21-473f-b8a8-3a0454166761','Configuracion-Roles',true),
	 ('cf10ed73-8cc8-4e28-8269-c93c5081e860','Configuracion-Archivos',true),
	 ('c27b5d5c-b0fc-48cd-8ba0-195763192c46','Configuracion-Usuarios',true),
	 ('6718c7ff-c431-4000-9477-e35562e15c96','VMI',true),
	 ('bfe4600c-3c8d-4c05-ac3a-260f1d9152e2','Home',true);
INSERT INTO "VMI-CPFR".permiso (id,nombre_permiso,activo) VALUES
	 ('4146e612-c411-456e-90c2-71dbc35a4e0c','Configuracion',true),
	 ('2ace12eb-5fb2-45ba-b1b4-457b0193b573','VMI-EditarParametros',true),
	 ('593f852f-bd14-40f2-98f0-c519dc805e8e','CPFR-todos_centros',true);
